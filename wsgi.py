"""This module provide project startup"""
from app import app

if __name__ == '__main__':
    app.run()
