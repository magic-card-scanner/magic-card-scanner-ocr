"""This module provide project configuration"""


class Config:  # pylint: disable=too-few-public-methods
    """This class provide constant configuration"""
    APP_NAME = "Magic Card Scanner OCR"
    UPLOAD_EXTENSIONS = ['.jpg', '.png', '.gif']
    UPLOAD_PATH = 'uploads'
