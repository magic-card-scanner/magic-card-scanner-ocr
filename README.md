
# Magic Card Scanner OCR (Optical Character Recognition)

This OCR service provides **Magic The Gathering** card scanning thanks to **Tesseract Open Source OCR Engine**.

You can send the picture of your card and the **Magic Card Scanner OCR** will treat the image to send back the name of the card in the **Magic Card Scanner REST API**.

## Install

```
pip install -r requirements.txt
```

## Run the app

```
python app.py
```

# OCR service

The OCR service will run on port `8081` by default.

## Get card name

### Request

`POST /scans`

This request will show the name of the card from a picture sent to the OCR service.

### Example

Sending a picture of the card named **"Liliana of the Dark Realms"** to the OCR service as a `form-data` object of type `File`.

![Magic Card](https://i.ibb.co/gwp8BWz/45b5a762-6636-500f-9459-94ee91a0e552.png)

At the end of the microservice processing, the image will be treated and cropped to only get the name of the card :

![Treated Magic Card](https://i.ibb.co/xsG9ncx/qdqdsqsd.png)

The OCR microservice will send back a json with the freshly recognized card name :

### Magic Card Scanner OCR Response

```
{
    "name": "lilianaofthedarkrealms"
}
```